<a name="unreleased"></a>
## [Unreleased]


<a name="v1.0.8"></a>
## [v1.0.8] - 2019-11-07
### Features
- add priority to send methods


<a name="v1.0.7"></a>
## [v1.0.7] - 2019-11-07
### Features
- add the possibility to set priority for a message


<a name="v1.0.6"></a>
## [v1.0.6] - 2019-09-03
### Features
- add access to context field


<a name="v1.0.5"></a>
## [v1.0.5] - 2019-09-03
### Code Refactoring
- remove library github.com/ibm-messaging/mq-golang-jms20


<a name="v1.0.4"></a>
## [v1.0.4] - 2019-09-02

<a name="v1.0.3"></a>
## [v1.0.3] - 2019-09-02

<a name="v1.0.2"></a>
## [v1.0.2] - 2019-08-21
### Code Refactoring
- update lib version gitlab.com/shadowy/go/utils

### Features
- add method GetQueueByName


<a name="v1.0.1"></a>
## v1.0.1 - 2019-07-23
### Features
- basic implementation


[Unreleased]: https://gitlab.com/shadowy/go/ibmq/compare/v1.0.8...HEAD
[v1.0.8]: https://gitlab.com/shadowy/go/ibmq/compare/v1.0.7...v1.0.8
[v1.0.7]: https://gitlab.com/shadowy/go/ibmq/compare/v1.0.6...v1.0.7
[v1.0.6]: https://gitlab.com/shadowy/go/ibmq/compare/v1.0.5...v1.0.6
[v1.0.5]: https://gitlab.com/shadowy/go/ibmq/compare/v1.0.4...v1.0.5
[v1.0.4]: https://gitlab.com/shadowy/go/ibmq/compare/v1.0.3...v1.0.4
[v1.0.3]: https://gitlab.com/shadowy/go/ibmq/compare/v1.0.2...v1.0.3
[v1.0.2]: https://gitlab.com/shadowy/go/ibmq/compare/v1.0.1...v1.0.2
