module gitlab.com/shadowy/go/ibmmq

go 1.12

require (
	github.com/ibm-messaging/mq-golang v0.0.0-20190820103725-19b946c185a8
	github.com/sirupsen/logrus v1.4.2
	gitlab.com/shadowy/go/utils v1.0.2
)
