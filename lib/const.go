package lib

// Used to configure messages to be sent Persistently.
const DeliveryMode_PERSISTENT int = 2

// Used to configure messages to be sent Non-Persistently.
const DeliveryMode_NON_PERSISTENT int = 1
