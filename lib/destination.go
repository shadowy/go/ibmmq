package lib

// Destination encapsulates a provider-specific address, which is typically
// specialized as either a Queue or a Topic.
type Destination interface {

	// GetDestinationName returns the name of the destination represented by this
	// object.
	//
	// In Java JMS this interface contains no methods and is only a parent for the
	// Queue and Topic interfaces, however in Golang an interface with no methods
	// is automatically implemented by every object, so we need to define at least
	// one method here in order to make it meet the JMS style semantics.
	GetDestinationName() string
}
