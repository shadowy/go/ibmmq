package lib

// QueueImpl encapsulates the provider-specific attributes necessary to
// communicate with an IBM MQ queue.
type QueueImpl struct {
	queueName string
}

// GetQueueName returns the provider-specific name of the queue that is
// represented by this object.
func (queue QueueImpl) GetQueueName() string {

	return queue.queueName

}

// GetDestinationName returns the name of the destination represented by this
// object.
func (queue QueueImpl) GetDestinationName() string {

	return queue.queueName

}
