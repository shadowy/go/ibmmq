package lib

import (
	"errors"
	"github.com/ibm-messaging/mq-golang/ibmmq"
	"strconv"
)

// ConnectionFactoryImpl defines a struct that contains attributes for
// each of the key properties required to establish a connection to an IBM MQ
// queue manager.
//
// The fields are defined as Public so that the struct can be initialised
// programmatically using whatever approach the application prefers.
type ConnectionFactoryImpl struct {
	QMName      string
	Hostname    string
	PortNumber  int
	ChannelName string
	UserName    string
	Password    string
}

// CreateContext implements the JMS method to create a connection to an IBM MQ
// queue manager.
func (cf ConnectionFactoryImpl) CreateContext() (ContextImpl, error) {

	// Allocate the internal structures required to create an connection to IBM MQ.
	cno := ibmmq.NewMQCNO()
	cd := ibmmq.NewMQCD()

	// Fill in the required fields in the channel definition structure
	cd.ChannelName = cf.ChannelName
	cd.ConnectionName = cf.Hostname + "(" + strconv.Itoa(cf.PortNumber) + ")"

	// Store the user credentials in an MQCSP, which ensures that long passwords
	// can be used.
	csp := ibmmq.NewMQCSP()
	csp.AuthenticationType = ibmmq.MQCSP_AUTH_USER_ID_AND_PWD
	csp.UserId = cf.UserName
	csp.Password = cf.Password

	// Join the objects together as necessary so that they can be provided as
	// part of the connect call.
	cno.ClientConn = cd
	cno.SecurityParms = csp

	// Indicate that we want to use a client (TCP) connection.
	cno.Options = ibmmq.MQCNO_CLIENT_BINDING

	var ctx ContextImpl
	var retErr error

	// Use the objects that we have configured to create a connection to the
	// queue manager.
	qMgr, err := ibmmq.Connx(cf.QMName, cno)

	if err == nil {

		// Connection was created successfully, so we wrap the MQI object into
		// a new ContextImpl and return it to the caller.
		ctx = ContextImpl{
			qMgr: qMgr,
		}

	} else {

		// The underlying MQI call returned an error, so extract the relevant
		// details and pass it back to the caller as a JMSException
		rcInt := int(err.(*ibmmq.MQReturn).MQRC)
		errCode := strconv.Itoa(rcInt)
		reason := ibmmq.MQItoString("RC", rcInt)
		retErr = errors.New(reason + " " + errCode + " " + err.Error())

	}

	return ctx, retErr

}
