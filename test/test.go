package main

import (
	"fmt"
	"github.com/sirupsen/logrus"
	"gitlab.com/shadowy/go/ibmmq"
	"gitlab.com/shadowy/go/utils"
)

func main() {
	ibm := ibmmq.IBMMq{
		QMName:   utils.PtrString("QM1"),
		Host:     "mq.avtologistika.com",
		Port:     utils.PtrInt(1415),
		Channel:  "DEV2",
		User:     "mqconn",
		Password: "567Bgtyhn5",
	}
	_ = ibm.Init()
	fmt.Printf("%#v\n", ibm)
	err := ibm.Open()
	if err != nil {
		logrus.WithError(err).Error("context")
	} else {
		defer ibm.Close()
	}

	fmt.Printf("----------------------------")
}
