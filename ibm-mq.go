package ibmmq

import (
	"github.com/sirupsen/logrus"
	"gitlab.com/shadowy/go/ibmmq/lib"
	"gitlab.com/shadowy/go/utils"
)

// IBMMq - ibm-mq configuration and provide streams
type IBMMq struct {
	QMName   *string `yaml:"qmName"`
	Host     string  `yaml:"host"`
	Port     *int    `yaml:"port"`
	User     string  `yaml:"user"`
	Password string  `yaml:"password"`
	Channel  string  `yaml:"channel"`

	connectionFactory *lib.ConnectionFactoryImpl
	context           *lib.ContextImpl
}

// Init - initialize properties
func (mq *IBMMq) Init() error {
	logrus.Debug("IBMMq.Init")
	if mq.Port == nil {
		mq.Port = utils.PtrInt(1414)
	}

	mq.connectionFactory = &lib.ConnectionFactoryImpl{
		Hostname:    mq.Host,
		PortNumber:  *mq.Port,
		ChannelName: mq.Channel,
		UserName:    mq.User,
		Password:    mq.Password,
	}
	if mq.QMName != nil {
		mq.connectionFactory.QMName = *mq.QMName
	}
	return nil
}

func (mq *IBMMq) Context() *lib.ContextImpl {
	return mq.context
}

// Open - open connection
func (mq *IBMMq) Open() (err error) {
	logrus.WithFields(mq.params()).Debug("IBMMq.Open")
	context, err := mq.connectionFactory.CreateContext()
	if err != nil {
		logrus.WithFields(mq.params()).WithError(err).Error("IBMMq.Open")
		return
	}
	mq.context = &context
	return
}

// Close - close connection
func (mq IBMMq) Close() {
	logrus.WithFields(mq.params()).Debug("IBMMq.Close")
	if mq.context == nil {
		return
	}
	mq.context.Close()
	mq.context = nil
}

// GetOutput - get output
func (mq *IBMMq) GetOutput() *lib.ProducerImpl {
	logrus.WithFields(mq.params()).Debug("IBMMq.GetOutput")
	return mq.context.CreateProducer()
}

//GetQueueByName - get queue by name
func (mq *IBMMq) GetQueueByName(dest string) *lib.QueueImpl {
	return mq.context.CreateQueue(dest)
}

// GetInput - get input
func (mq *IBMMq) GetInput(dest string, params ...string) (result *lib.ConsumerImpl, err error) {
	logrus.WithFields(mq.params()).Debug("IBMMq.GetInput")
	var consumer *lib.ConsumerImpl
	if len(params) == 0 {
		consumer, err = mq.context.CreateConsumer(mq.context.CreateQueue(dest))
	} else {
		consumer, err = mq.context.CreateConsumerWithSelector(mq.context.CreateQueue(dest), params[0])
	}
	if err != nil {
		return
	}
	result = consumer
	return
}

func (mq *IBMMq) params() logrus.Fields {
	return logrus.Fields{
		"QMName":      mq.connectionFactory.QMName,
		"Hostname":    mq.connectionFactory.Hostname,
		"Port":        mq.connectionFactory.PortNumber,
		"ChannelName": mq.connectionFactory.ChannelName,
		"UserName":    mq.connectionFactory.UserName,
	}
}
